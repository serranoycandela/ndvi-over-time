library(geojsonsf)
library(dplyr)
library(sf)
library(tidyr)
library(ggplot2)
library(gridGraphics)
library(ggsci)
library(ggpubr)
library(cowplot)

geo <- geojson_sf("/home/fidel/GitLab/ndvi-over-time/anps_modis2.geojson")

df_wide <- geo %>%
  st_drop_geometry() 

#df_wide = subset(df_wide, select = -c(a_bosque_6,p_bosque_6,a_bosque_7,p_bosque_7,area,pdif_6_7) )

data_long <- gather(df_wide, fecha, value, c(starts_with("20")), factor_key=TRUE)

data_long$year <- as.numeric(substring(data_long$fecha,1,4))

data_long$month <- as.numeric(substring(data_long$fecha,6,7))

# Group by mean using dplyr
agg_tbl <- data_long %>% group_by(year, Nombre) %>% 
  summarise(NDVI=mean(value),
            .groups = 'drop')

pal <- c("#800000B2", "#ca80ff", "#FFA319B2", "#8A9045B2", "#155F83B2")

p1 <- ggplot(agg_tbl, aes(x = year, y = NDVI, color = Nombre)) +
  geom_line() +
  xlim(2000, 2023) +
  scale_color_manual(values = pal)+
  guides(color=guide_legend(nrow=2,byrow=TRUE))+
  theme(legend.position = "bottom")

marzos <- data_long %>% filter(month == "3")
names(marzos)[3] <- "NDVI"
ggplot(marzos, aes(x = year, y = NDVI, color = Nombre)) +
  geom_line() +
  xlim(2000, 2023) +
  theme(legend.position = "bottom")

agostos <- data_long %>% filter(month == "8")
names(agostos)[3] <- "NDVI"
ggplot(agostos, aes(x = year, y = NDVI, color = Nombre)) +
  geom_line() +
  xlim(2000, 2023) +
  theme(legend.position = "bottom")


pal <- c("#800000B2", "#ca80ff", "#FFA319B2", "#8A9045B2", "#155F83B2")

library(maptiles)
library(tidyterra)

bb <- st_bbox(geo)

mex <- matrix(bb, 2, byrow = TRUE) |>
  st_multipoint()       |> 
  st_sfc(crs = 4326)    |>
  st_transform(3857) |>
  get_tiles(provider = "CartoDB.Positron", zoom = 7, crop = TRUE)

map <- ggplot() +
  geom_spatraster_rgb(data = mex) +
  geom_sf(data = geo, mapping = aes(fill = Nombre), color = "white", size = 0.1, show.legend = FALSE) +
  scale_fill_manual(values = pal) +
  theme_void()



fig1 <- plot_grid(map, p1, nrow = 2, ncol = 1, labels="auto")

ggsave(fig1, 
       file="/home/fidel/GitLab/ndvi-over-time/map_plot.png", 
       width = 19, 
       height = 14, 
       dpi = 300,
       bg = "white",
       units = "cm", 
       device='png')
