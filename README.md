# NDVI over time



## ¿Que hace?

Calcular el promedio del índice de verdor conocido como NDVI a lo largo del tiempo para uno o mas polígonos.


## ¿Como lo hace?

Mediante 3 scripts, uno que corre en google earth engine, otro que corre dentro de QGIS y el último que corre en R.

### Google Earth Engine

Este script está programado en javascript y su proposito es calcular el índice NDVI para cada celda de cada escena en un periodo de tiempo. Las imágenes que se usan de insumo para estos cálculos son de los satélites Landsat. El resultado de correr este script son una serie de rasters en formato tif que contienen dicho índice.

### QGIS

Este script está programado en python y su proposito es calcular el promedio de las celdas no nulas que forman parte de un polígono dado. El resultado de correr este script es un archivo csv que contine esos promedios, es decir un valor promedio por cada imagen en la lista de imagenes producidas en el paso anterior.

### R

Este script está programado en R y su proposito es promediar los valores para cada año y generar una gráfica de linea, el resultado de correr este script es la gráfica de linea que representa la evolución temporal del NDVI en el sitio de estudio.

## Caso Tanaco

El polígono para este ejemplo fue la extensión de un cerro al norte del poblado Tanaco en Michoacán.
![Alternative text](cerca.png "Un polígono que abarca la extensión de un cerro al norte del poblado Tanaco en Michoacán")

![Alternative text](lejos.png "Un polígono que abarca la extensión de un cerro al norte del poblado Tanaco en Michoacán")

Y el resultado de correr los 3 scripts fue la sigueinte gráfica de linea que muestra la evolución temporal del NDVI en el cerro al norte de Tanaco.

![Alternative text](time_plot.png "Cálculo del NDVI para un polígono que abarca la extensión de un cerro al norte del poblado Tanaco en Michoacán")


De 2013 a 2016 se observa un aumento mientras que a partir del 2016 en general baja con algunas fluctuaciones. Considerando lo anterior, a continuación se muestra el NDVI para 3 imágenes que corresponden al mes abril de 2013, 2016 y 2022.

### NDVI del 9 de abril del 2013
![Alternative text](2013.png "2013 NDVI")


### NDVI del 24 de abril del 2016
![Alternative text](2016.png "2016 NDVI")


### NDVI del 25 de abril del 2022
![Alternative text](2022.png "2022 NDVI")

### Animación
![Alternative text](anim.gif "animación")



## Procesar en Google Earth Engine, y visiualizar en R

![Alternative text](map_plot.png "modis collection")
