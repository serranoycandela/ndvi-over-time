
// code developed by Kevin Galván Lara
//
//////////////////////////////////////////////////////////////////

var batch = require('users/fitoprincipe/geetools:batch');
function addNDVI(image){
  var NDVI = image.normalizedDifference(["SR_B4","SR_B5"]).rename("NDVI");
  return image.addBands(NDVI);
}
//function addNBR(image){
//  var NBR = image.normalizedDifference(["SR_B5","SR_B7"]).rename("NBR");
//  return image.addBands(NBR);
//}

function noData(image) {
  return image.unmask(-9999);
}
/**
 * Function to mask clouds and snow based on the pixel_qa band of Landsat 8 SR data.
 * @param {ee.Image} image input Landsat 8 SR image
 * @return {ee.Image} cloudmasked Landsat 8 image
 */
function nubes(image) {
  // Bits 2, 3, 4 and 5 are water, cloud shadow, cloud, and snow, respectively.
  var cloudShadowBitMask = (1 << 4);
  var cloudBitMask = (1 << 3);
  var snowBitMask = (1 << 5);
  var waterBitMask = (1 << 7);
  // Get the pixel QA band.
  var pixelQA = image.select('QA_PIXEL');
  // Both flags should be set to zero, indicating clear conditions.
  var mask = pixelQA.bitwiseAnd(cloudShadowBitMask).eq(0)
                 .and(pixelQA.bitwiseAnd(cloudBitMask).eq(0))
                 .and(pixelQA.bitwiseAnd(snowBitMask).eq(0))
                 .and(pixelQA.bitwiseAnd(waterBitMask).eq(0));
  return image.updateMask(mask);
}

// Aplicar escala
function applyScaleFactors(image) {
  var opticalBands = image.select('SR_B.').multiply(0.0000275).add(-0.2);
  var thermalBands = image.select('ST_B.*').multiply(0.00341802).add(149.0);
  return image.addBands(opticalBands, null, true)
              .addBands(thermalBands, null, true);
}

var landsat = ee.ImageCollection("LANDSAT/LC08/C02/T1_L2") //"." es la continuacion en cadena del resultado en la sig funcion
  .filterDate("2013-01-01", "2022-12-31")// filtrar por fechas
  .filter(ee.Filter.lt('CLOUD_COVER', 15))//filtrar las nubosas
  //.filter(ee.Filter.calendarRange(3, 3, 'month'))
  .filterBounds(poligono) // Filtar por el poligono
  .map(applyScaleFactors) // Aplixar funcion a varios elementos, en este caso se aplica antes del NDVI para que haga los calculos y se estandarice
  .map(nubes)
  .map(addNDVI)
  //.map(addNBR)
  //.map(noData)
  .select("NDVI") //, "NDVI")
 ;
print('prueba', landsat);

Map.addLayer(landsat.select("NDVI"))
//Map.addLayer(landsat.select("NBR"))

var options = {
      scale: 30,
      //maxPixels: 1e13,
      type: 'float',
      region: poligono,
      name: '{id}',
     // crs: null,
      dateFormat: 'yyyy-MM-dd',
     // async: false
    };

batch.Download.ImageCollection.toDrive(landsat, "ndvi-carla", options);
