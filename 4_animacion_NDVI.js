// Make a day-of-year sequence from 1 to 365 with a 16-day step.
var years = ee.List.sequence(2000,2023);

// Import a MODIS NDVI collection.
//var ndviCol = ee.ImageCollection('MODIS/006/MOD13A2').select('NDVI');
var comp = ee.ImageCollection('MODIS/061/MOD13A3').select('NDVI');

var yearlyImage = function(beginingYear){
  var startDate = ee.Date.fromYMD(beginingYear,1,1);
  var endDate = startDate.advance(1, 'year');
  var thisYear = comp.filter(ee.Filter.date(startDate,endDate));
  var yearly_mean = thisYear.reduce(ee.Reducer.mean());
  return yearly_mean.set({'system:time_start': startDate.millis(),
                          'system:time_end': endDate.millis(),
                          'year': beginingYear
  })
}

var yearlylist = years.map(yearlyImage);
var yearly = ee.ImageCollection.fromImages(yearlylist);

// Define RGB visualization parameters.
var visParams = {
  min: 0.0,
  max: 9000.0,
  palette: [
    'FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718', '74A901',
    '66A000', '529400', '3E8601', '207401', '056201', '004C00', '023B01',
    '012E01', '011D01', '011301'
  ],
};
// Define a mask to clip the NDVI data by.
var mask = ee.FeatureCollection('USDOS/LSIB_SIMPLE/2017')
  .filter(ee.Filter.eq('wld_rgn', 'North America'));

// Define the regional bounds of animation frames.
var region = ee.Geometry.Polygon(
  [[[-100, 21],
    [-100, 15],
    [-93, 15],
    [-93, 21]]],
  null, false
);

var text = require('users/gena/packages:text');


var textProperties = {
  fontSize: 32,
  textColor: 'ffffff',
  outlineColor: '000000',
  outlineWidth: 0,
  outlineOpacity: 0.6
};


// Create RGB visualization images for use as animation frames.
var rgbVis = yearly.map(function(img) {
  var label = ee.Number(img.get('year')).toInt();
  var scale = 2000//19567;
  var geometryLabel = ee.Geometry.Point([-94.5, 20.5]);
  var yeartext = text.draw(label, geometryLabel, scale, {fontSize: 32});
  return img.visualize(visParams).clip(mask).blend(yeartext);
});

// Define GIF visualization parameters.
var gifParams = {
  'region': region,
  'dimensions': 800,
  'crs': 'EPSG:3857',
  'framesPerSecond': 2
};

// Print the GIF URL to the console.
print(rgbVis.getVideoThumbURL(gifParams));

print(ui.Thumbnail(rgbVis, gifParams));
