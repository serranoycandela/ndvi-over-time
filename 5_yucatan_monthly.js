// Make a day-of-year sequence from 1 to 365 with a 16-day step.
var months = ee.List.sequence(1,12);

// Import a MODIS NDVI collection.
//var ndviCol = ee.ImageCollection('MODIS/006/MOD13A2').select('NDVI');
var comp = ee.ImageCollection('MODIS/061/MOD13A3').select('NDVI');

var monthlyImage = function(beginingMonth){
  var startDate = ee.Date.fromYMD(2000,beginingMonth,1);
  var endDate = startDate.advance(1, 'month');

  var thisMonth = comp.filter(ee.Filter.calendarRange(beginingMonth, beginingMonth, 'month'));
  var monthly_mean = thisMonth.reduce(ee.Reducer.median());
  return monthly_mean.set({'system:time_start': startDate.millis(),
                          'system:time_end': endDate.millis(),
                          'month': beginingMonth
  })
}

var monthlylist = months.map(monthlyImage);
var monthly = ee.ImageCollection.fromImages(monthlylist);

// Define RGB visualization parameters.
var visParams = {
  min: 0.0,
  max: 9000.0,
  palette: [
    'FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718', '74A901',
    '66A000', '529400', '3E8601', '207401', '056201', '004C00', '023B01',
    '012E01', '011D01', '011301'
  ],
};
// Define a mask to clip the NDVI data by.
var mask = ee.FeatureCollection('USDOS/LSIB_SIMPLE/2017')
  .filter(ee.Filter.eq('wld_rgn', 'North America'));

// Define the regional bounds of animation frames.
var region = ee.Geometry.Polygon(
  [[[-91, 22],
    [-91, 18],
    [-86, 18],
    [-86, 22]]],
  null, false
);

var text = require('users/gena/packages:text');


var textProperties = {
  fontSize: 32,
  textColor: 'ffffff',
  outlineColor: '000000',
  outlineWidth: 0,
  outlineOpacity: 0.6
};


// Create RGB visualization images for use as animation frames.
var rgbVis = monthly.map(function(img) {
  var m = ee.Number(img.get('month')).toInt();
  var date = ee.Date.fromYMD(2000,m,1);
  var label = date.format('MMMM')
  var scale = 2000//19567;
  var geometryLabel = ee.Geometry.Point([-88.5, 18.5]);
  var yeartext = text.draw(label, geometryLabel, scale, {fontSize: 32});
  return img.visualize(visParams).clip(mask).blend(yeartext);
});

// Define GIF visualization parameters.
var gifParams = {
  'region': region,
  'dimensions': 800,
  'crs': 'EPSG:3857',
  'framesPerSecond': 1
};

// Print the GIF URL to the console.
print(rgbVis.getVideoThumbURL(gifParams));

print(ui.Thumbnail(rgbVis, gifParams));
