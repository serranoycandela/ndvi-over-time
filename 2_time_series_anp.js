var poligonos = anps.filter(ee.Filter.or(ee.Filter.eq('Nombre','Corredor Biológico Chichinautzin'),
                                              ee.Filter.eq('Nombre','Z.P.F.T.C.C. de los ríos Valle de Bravo, Malacatepec, Tilostoc y Temascaltepec'),
                                              ee.Filter.eq('Nombre',"Bala'an K'aax"),
                                              ee.Filter.eq('Nombre','Montes Azules'),
                                              ee.Filter.eq('Nombre',"El Triunfo"),
                                              ee.Filter.eq('Nombre',"El Tepeyac")));

var ndvi = ee.ImageCollection("LANDSAT/COMPOSITES/C02/T1_L2_ANNUAL_NDVI")
            .filterBounds(poligonos)
            .filterDate('2000-01-01', '2023-01-01');


//var ndvi_modis = ee.ImageCollection("MODIS/061/MOD13A3")
//            .filterBounds(poligonos)
//            .filterDate('2000-02-01', '2024-07-01');

var chart = ui.Chart.image.seriesByRegion({
  imageCollection: ndvi,
  band: 'NDVI',
  regions: poligonos,
  reducer: ee.Reducer.mean(),
  scale: 540,
  seriesProperty: 'Nombre',
  xProperty: 'system:time_start'
})
.setOptions({
  title: 'Average NDVI (LANDSAT) Value by Date',
  hAxis: {title: 'Date', titleTextStyle: {italic: false, bold: true}},
  vAxis: {
    title: 'NDVI',
    titleTextStyle: {italic: false, bold: true}
  },
  lineWidth: 3,
});
print(chart);
