var poligonos = anps.filter(ee.Filter.or(ee.Filter.eq('Nombre','Corredor Biológico Chichinautzin'),
                                              ee.Filter.eq('Nombre','Z.P.F.T.C.C. de los ríos Valle de Bravo, Malacatepec, Tilostoc y Temascaltepec'),
                                              ee.Filter.eq('Nombre',"Bala'an K'aax"),
                                              ee.Filter.eq('Nombre','Montes Azules'),
                                              ee.Filter.eq('Nombre',"El Triunfo")));

Map.addLayer(poligonos);
var ndvi_modis = ee.ImageCollection("MODIS/061/MOD13A3")
            .filterBounds(poligonos)
            .filterDate('2000-02-01', '2024-07-01');

var reduced = ndvi_modis.map(function(image){
  return image.divide(10000).reduceRegions({
    collection:poligonos ,
    reducer:ee.Reducer.mean(),
    scale: 1000
  });
});
print(reduced);

var n = reduced.getInfo().features.length;
var list = reduced.toList(n,0)

var toyFilter = ee.Filter.equals({
    leftField: 'Nombre',
    rightField: 'Nombre'
  });

function cleanJoin(feature){
    return ee.Feature(feature.get('primary')).copyProperties(feature.get('secondary'));
  }

var i = 0;
for(i=0;i<n;i++){
  var i_n = ee.FeatureCollection(list.get(i)).getInfo().features.length;
  var date = ee.FeatureCollection(list.get(i)).get("system:index");
  var esta_fecha = ee.FeatureCollection(list.get(i)).select(['Nombre', 'NDVI'], ['Nombre', date]);
  var innerJoin = ee.Join.inner();
  var toyJoin = innerJoin.apply(poligonos, esta_fecha, toyFilter);
  poligonos = toyJoin.map(cleanJoin);
}

print(poligonos);
Export.table.toDrive(poligonos);
