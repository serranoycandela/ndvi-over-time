import os
import pandas as pd

# https://code.earthengine.google.com/?scriptPath=users%2Fserranoycandela74%2Fndvi-lancis%3Andvi-script



path_ndvi = "/home/fidel/Documents/tanaco"

lista_tifs = os.listdir(path_ndvi)

cerro_path = "/home/fidel/Documents/tanaco/cerro_tanaco.shp"

cerro = QgsVectorLayer(cerro_path,"cerro","ogr")



for r in lista_tifs:
    if r.endswith(".tif"):
        r_path = os.path.join(path_ndvi, r)
        print(r_path)
        out_path = "/home/fidel/Documents/tanaco/temp/"+r.replace(".tif", ".shp")

        params = {"INPUT": cerro, "INPUT_RASTER": r_path, "OUTPUT": out_path, "RASTER_BAND": 1}
        processing.run("native:zonalstatisticsfb", params)



temp_path = "/home/fidel/Documents/tanaco/temp"
lista_shps = os.listdir(temp_path)
df = pd.DataFrame(columns = ['year', 'value'])
for f in lista_shps:
    year = f.split(".")[0].split("_")[2][:4]
    if f.endswith(".shp"):
        f_path = os.path.join(temp_path, f)

        mean_ndvi = QgsVectorLayer(f_path,"ndvi","ogr")
        if next(mean_ndvi.getFeatures())["_mean"]:
            df = df.append({'year' : year, 'value' : next(mean_ndvi.getFeatures())["_mean"]},
                        ignore_index = True)
            print(next(mean_ndvi.getFeatures())["_mean"], year)

filepath = "/home/fidel/Documents/tanaco/ndvi_tanaco.csv"
df.to_csv(filepath, index=False)
