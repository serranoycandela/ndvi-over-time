var poligonos =municipios.filter(ee.Filter.or(ee.Filter.eq('Nombre','Tenango del Valle'),
                                              ee.Filter.eq('Nombre','Tlatlaya')));

var ndvi = ee.ImageCollection("LANDSAT/COMPOSITES/C02/T1_L2_ANNUAL_NDVI")
            .filterBounds(poligonos)
            .filterDate('2000-01-01', '2023-12-01');

//var ndvi = ee.ImageCollection("MODIS/061/MOD13A3")
//            .filterBounds(poligonos)
//            .filterDate('2000-02-01', '2023-12-01');

var chart = ui.Chart.image.seriesByRegion({
  imageCollection: ndvi,
  band: 'NDVI',
  regions: poligonos,
  reducer: ee.Reducer.mean(),
  //scale: 500,
  seriesProperty: 'Nombre',
  xProperty: 'system:time_start'
}).setOptions({
  title: 'Average NDVI Value by Date',
  hAxis: {title: 'Date', titleTextStyle: {italic: false, bold: true}},
  vAxis: {
    title: 'NDVI',
    titleTextStyle: {italic: false, bold: true}
  },
  lineWidth: 5,
});
print(chart);
